<?php
namespace OCA\SlackNotify\AppInfo;

use OCP\AppFramework\App;
use OCP\IL10N;
use OCP\Util;

$app = new App('slacknotify');
$container = $app->getContainer();
$container->query('OCP\INavigationManager')->add(function () use ($container) {
    $urlGenerator = $container->query('OCP\IURLGenerator');
    $l10n = $container->query('OCP\IL10N');
    return [
        'id' => 'slacknotify',
        'order' => 10,
        'href' => $urlGenerator->linkToRoute('slacknotify.page.index'),
        'icon' => $urlGenerator->imagePath('slacknotify', 'app.svg'),
        'name' => $l10n->t('Slack Notify'),
    ];
});

$container->registerService('Logger', function($c) {
    return $c->query('ServerContainer')->getLogger();
});

$container->registerService('FileHooks', function($c) {
    return new FileHooks(
        $c->query('ServerContainer')->getRootFolder(),
        $c->query('Logger'),
        "SlackNotify"
    );
});

$filehooks = $container->query('FileHooks');
$filehooks->registerPostDelete();
$filehooks->registerPostRename();
