<?php
namespace OCA\SlackNotify\AppInfo;

use OCP\AppFramework\App;
use OCP\Util;
use OCP\ILogger;

class FileHooks {

	private $rootFolder;
	private $logger;
	private	$appName;

	public function __construct($rootFolder, ILogger $logger, $appName) {
		$this->rootFolder = $rootFolder;
		$this->logger = $logger;
		$this->appName = $appName;
	}

	public function log($message) {
		$this->logger->debug($message, array('app' => $this->appName));
	}

    public function registerPostDelete() {
		$reference = $this;

		$callback = function(\OCP\Files\Node $node) use($reference){
			$reference->log("SlackNotify: PostDelete"); 
			// $reference->log("SlackNotify: ".($node->GetName()));
		};

		$this->log("Registered postDelete");  // this is being shown in the log

		$this->rootFolder->listen('\OC\Files', 'postDelete', $callback);
    }

	public function registerPostRename() {
		$reference = $this;

		$callback = function($source, $target) use($reference){
			\OC::$server->getLogger()->debug("SlackNotify: postRename", array('app' => 'XXX')); 
			// $reference->log("SlackNotify: ".($source->GetName())); 
			// $reference->log("SlackNotify: ".($target->GetName())); 
		};

		$this->log("Registered postRename");  // this is being shown in the log

		$this->rootFolder->listen('\OC\Files', 'postRename', $callback);
    }
}